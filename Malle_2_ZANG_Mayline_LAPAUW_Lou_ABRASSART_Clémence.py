# coding: utf8

'''
Mini-projet "Harry se fait enfin la malle" deuxième partie avec
les réponses qui s'affichent dans la console.

Auteurs: Lou LAPAUW, Clémence ABRASSART, Mayline ZHANG

Licence (CC BY-NC-SA 3.0 FR)
https://creativecommons.org/licenses/by-nc-sa/3.0/fr/
VERSION FINALE
24/02/2021

'''

import copy

# CONSTANTES

FOURNITURES_SCOLAIRES = \
[{'Nom': 'Manuel scolaire', 'Poids': 0.55, 'Mana': 11},
 {'Nom': 'Baguette magique', 'Poids': 0.085, 'Mana': 120},
 {'Nom': 'Chaudron', 'Poids': 2.5, 'Mana': 2},
 {'Nom': 'Boîte de fioles', 'Poids': 1.2, 'Mana': 4},
 {'Nom': 'Téléscope', 'Poids': 1.9, 'Mana': 6},
 {'Nom': 'Balance de cuivre', 'Poids': 1.3, 'Mana': 3},
 {'Nom': 'Robe de travail', 'Poids': 0.5, 'Mana': 8},
 {'Nom': 'Chapeau pointu', 'Poids': 0.7, 'Mana': 9},
 {'Nom': 'Gants', 'Poids': 0.6, 'Mana': 25},
 {'Nom': 'Cape', 'Poids': 1.1, 'Mana': 13}]

POIDS_MAXIMAL = 4

# DEFFINITIONS DES FONCTIONS:
# ETAPE 1

def remplir_a(liste_fourniture, poids_max):
    """
    Fonction qui remplit une malle selon les derniers objets rajoutés dans
    une liste.
    Entrée: liste de dictionnaires fournitures
    Sortie: une liste de dictionnaires des contenants de la malle
    """
    caisse = []
    liste = copy.deepcopy(liste_fourniture)
    poids = 0
    for _ in range(len(liste)):
        object = liste.pop()
        if poids + object['Poids'] <= poids_max:
            caisse.append(object)
            poids += object['Poids']
    return caisse


def reponse_a(liste_malle):
    """
    Fonction qui permet de créer une réponse à la fonction
    remplir_a.
    Entrée : liste de dictionnaires
    Sortie: une chaine de caractères
    """
    poids_de_la_malle = 0
    texte = 'La malle contient : \n'
    for element in liste_malle:
        texte += f" - un(e) {element['Nom']} qui pèse {element['Poids']}, \n"
        poids_de_la_malle += element['Poids']
    texte += f'et pèse {poids_de_la_malle}\n'
    return texte


# ETAPE 2


def liste_binaire(nombre):
    """
    Fonction qui permet de gerner une liste de tous les nombres possibles
    binaires jusqu'à un nombre choisi.

    Entrée: un entier
    Sortie: une liste avec tous les nombres binaires jusqu'au nombre choisi
    """
    liste = []
    for i in range(2 ** nombre):
        nb_binaire = bin(i)[2:]
        nb_binaire = (nombre - len(nb_binaire)) * '0' + nb_binaire
        # création de tous les nombres binaires
        liste.append(nb_binaire)
    return liste

# on va utilser cette liste de nombres binaires pour créer une fonction
# de toutes les possibilités des élements dans la liste de fourniture


def liste_combinaisons(fourniture):
    """
    Créer une liste avec toutes les combinaisons possibles
    des élements dans une liste.
    Entrée : une liste avec les élements nécessaires
    Sortie: une liste avec toutes les combinaisons possibles
    """
    binaires = liste_binaire(len(fourniture))
    # on va créer toutes les combinaisons de
    # nombres binaires allant de 0 à la longueur de la liste
    combinaisons = []
    for nombre in binaires:
        combinaison = []
        for i, bit in enumerate(nombre):
            if bit == '1':          # si le nombre est un 1, on prend l'objet
                combinaison.append(fourniture[i])
        combinaisons.append(combinaison)
    return combinaisons


def reponse_b(malle):
    """
    Permet d'afficher une réponse à l'étape b.
    Entrée : une liste avec les contenants d'une malle et son poids
    Sortie: une chaine de caractères
    """
    texte = f'La malle la plus lourde possible pèse {malle[1]}\net contient:\n'
    for element in malle[0]:
        texte += f' - un(e) {element[0]} qui pèse {element[1]} \n'
    return texte

# ETAPE 3


def tri_c(tab):
    """
    Fonction qui trie une liste par mana décroissant.
    Entrée: une liste
    Sortie: une liste triée
    """
    for i in range(1, len(tab)):
        while tab[i]['Mana'] > tab[i - 1]['Mana'] and i > 0:
            tab[i], tab[i - 1] = tab[i - 1], tab[i]
            i = i - 1
    return tab


def malle_c(fourniture, max):
    """
    Fonction qui remplit la malle avec le plus de mana sans dépasser le poids.
    Entrée: une liste et un entier pour le poids maximum
    Sortie: un tuple avec: une liste de ce qu'il y a dans la malle
    (malle_magique) et un entier pour le mana qu'il y a dedans(mana)
    """
    malle_magique = []
    liste = tri_c(fourniture)
    poids = 0
    mana = 0
    for element in liste:
        if poids + element['Poids'] <= max:
            malle_magique.append(element)
            poids += element['Poids']
            mana += element['Mana']
    return malle_magique, mana


def reponse_c(resultat):
    """
    Permet d'afficher une réponse à l'étape b.
    Entrée : un tuple avec une liste de dictionnaires et un entier
    Sortie: une chaine de caractères
    """
    poids_malle = 0
    texte = 'La malle avec le plus de mana contient:\n'
    for element in resultat[0]:
        texte += f'- un(e) {element["Nom"]} qui à un mana de {element["Mana"]} \
et pèse {element["Poids"]}\n'
        poids_malle += element ['Poids']
    texte += f'Cela fait un mana de {resultat[1]} \
et un poids total de {poids_malle}.'
    return texte


# PROGRAMME


# étape 1

print("a) Remplir la malle... N'importe comment ! \n")

print(reponse_a(remplir_a(FOURNITURES_SCOLAIRES, POIDS_MAXIMAL)))

# étape 2

print('b) Remplir la malle la plus lourde possible. \n')
liste_poids = []
for object in FOURNITURES_SCOLAIRES:
    liste_poids.append([object['Nom'], object['Poids']])

liste_des_combinaisons = liste_combinaisons(liste_poids)
combinaisons_possibles = []

# calcul du poids obtenu par chaque combinaison

for combinaison in liste_des_combinaisons:
    poids_total = 0
    for element in combinaison:
        poids_total += element[1]
    if poids_total <= POIDS_MAXIMAL:
        combinaisons_possibles.append((combinaison, poids_total))

# comparaison du poids de chaque combinaison pour trouver le plus proche

plus_proche = 0
min = 1
poids_minimum = 0
for poids in combinaisons_possibles[1:]:
    if POIDS_MAXIMAL - poids[1] < min:
        min = POIDS_MAXIMAL - poids[1]
        plus_proche = poids[1]
        meilleure_combinaison = poids
print(reponse_b(meilleure_combinaison))

# étape 3

print('c) Remplir la malle avec le maximum de mana.\n')

print(reponse_c(malle_c(FOURNITURES_SCOLAIRES, POIDS_MAXIMAL)))
