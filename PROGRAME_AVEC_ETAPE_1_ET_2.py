# coding: utf8

'''
Mini-projet "Harry se fait la malle" deuxième partie avec
les réponses qui s'affichent dans la console

Auteurs: Lou LAPAUW, Clémence ABRASSART, Mayline YHANG

Licence (CC BY-NC-SA 3.0 FR)
https://creativecommons.org/licenses/by-nc-sa/3.0/fr/
VERSION FINALE
24/02/2021

'''


# DEFFINITIONS DES FONCTIONS:
# ETAPE 1
import copy


def remplir_a(liste_fourniture, poids_max):
    """
    Fonction qui remplit un malle selon les derniers objects rajoutés dans
    une liste.
    Entrée: liste de dictionnaires fournitures
    Sortie: une liste de dictionnaire des contenants de la malle
    """
    caisse = []
    liste = copy.deepcopy(liste_fourniture)
    poids = 0
    for _ in range(len(liste)):
        object = liste.pop()
        if poids + object['Poids'] <= poids_max:
            caisse.append(object)
            poids += object['Poids']
    return caisse


def reponse(liste_malle):
    """
    Fonction qui permet de créer une réponse
    Entrée : liste de dictionnaires
    Sortie: une chaine de caractères
    """
    poids_de_la_malle = 0
    texte = 'La malle contient : \n'
    for element in liste_malle:
        texte += f" - un(e) {element['Nom']} qui pèse {element['Poids']}, \n"
        poids_de_la_malle += element['Poids']
    texte += f'et pèse {poids_de_la_malle}\n'
    return texte


# ETAPE 2


def liste_binaire(nombre):
    """
    Fonction qui permet de gerner une liste de tous les nombre possible
    binaires jusqu'à un nombre choisi

    Entrée: un entier
    sortie: une liste avec tous les nombre binaire jusqu'au nombre
    """
    liste = []
    for i in range(2 ** nombre):
        nb_binaire = bin(i)[2:]
        nb_binaire = (nombre - len(nb_binaire)) * '0' + nb_binaire
        # création de tous les nombre binaires
        liste.append(nb_binaire)
    return liste

# on va utilser cette liste de nombre binaires pour creer un fonction
# de toutes les possibilités possibles des élement dans la fourniture


def liste_combinaisons(fourniture):
    """
    Creer une liste avec toutes les combinaisons possibles
    avec les élement dans une liste
    Entrée : une liste avec les élements nécessaires
    Sortie: une liste avec toutes les combinaisons possibles
    """
    binaires = liste_binaire(len(fourniture))
    # on va créer toutes les combinaisons de
    # nombre binaire allant de 0 à la longueure de la liste
    combinaisons = []
    for nombre in binaires:
        combinaison = []
        for i, bit in enumerate(nombre):
            if bit == '1':          # si le nombre est un 1, on prend l'object
                combinaison.append(fourniture[i])
        combinaisons.append(combinaison)
    return combinaisons


def reponse_b(malle):
    """
    Permet d'afficher une réponse à l'etape b
    Entrée : une liste avec les contenants d'une malle
    Sorite: une chaine de caractère
    """
    texte = f'La malle la plus lourde possible pèse {malle[1]}\net contient:\n'
    for element in malle[0]:
        texte += f' - un(e) {element[0]} qui pèse {element[1]} \n'
    return texte

# ETAPE 3


def tri_c(tab):
    """
    Fonction qui tri une liste par mana décroissant.
    Entrée: une liste
    Sortie: une liste triée
    """
    for i in range(1, len(tab)):
        while tab[i]['Mana'] > tab[i - 1]['Mana'] and i > 0:
            tab[i], tab[i - 1] = tab[i - 1], tab[i]
            i = i - 1
    return tab


def malle_c(fourniture, max):
    """
    Fonction remplit la malle avec le plus de mana sans dépasser le poids.
    Entrée: une liste(fourniture) et un entier pour le poids maximum (max)
    Sortie: un tuple avec: une liste de ce qu'il y a dans la malle
    (malle_magique) et un entier pour le mana qu'il y a dedans(mana)
    """
    malle_magique = []
    liste = tri_c(fourniture)
    poids = 0
    mana = 0
    for element in liste:
        if poids + element['Poids'] <= max:
            malle_magique.append(element)
            poids += element['Poids']
            mana += element['Mana']
    return malle_magique, mana


def reponse_c(resultat, poids_max):
    texte = 'La malle avec le plus de mana contient:\n'
    for element in resultat[0]:
        texte += f'- un(e) {element["Nom"]} qui à un mana de {element["Mana"]} \
et pèse {element["Poids"]}\n'
    texte += f'Cela fait un mana de {resultat[1]}.'
    return texte


# PROGRAMME

fournitures_scolaires = \
[{'Nom': 'Manuel scolaire', 'Poids': 0.55, 'Mana': 11},
 {'Nom': 'Baguette magique', 'Poids': 0.085, 'Mana': 120},
 {'Nom': 'Chaudron', 'Poids': 2.5, 'Mana': 2},
 {'Nom': 'Boîte de fioles', 'Poids': 1.2, 'Mana': 4},
 {'Nom': 'Téléscope', 'Poids': 1.9, 'Mana': 6},
 {'Nom': 'Balance de cuivre', 'Poids': 1.3, 'Mana': 3},
 {'Nom': 'Robe de travail', 'Poids': 0.5, 'Mana': 8},
 {'Nom': 'Chapeau pointu', 'Poids': 0.7, 'Mana': 9},
 {'Nom': 'Gants', 'Poids': 0.6, 'Mana': 25},
 {'Nom': 'Cape', 'Poids': 1.1, 'Mana': 13}]

poids_maximal = 4


# ETAPE 1

print("a) Remplir la malle... N'importe comment ! \n")
print(reponse(remplir_a(fournitures_scolaires, poids_maximal)))

# ETAPE 2

print('b) Remplir la malle la plus lourde possible. \n')
liste_poids = []
for object in fournitures_scolaires:
    liste_poids.append([object['Nom'], object['Poids']])

liste_des_combinaisons = liste_combinaisons(liste_poids)
combinaisons_possibles = []

# calcul du poids obtenus par chaque combinaison

for combinaison in liste_des_combinaisons:
    poids_total = 0
    for element in combinaison:
        poids_total += element[1]
    if poids_total <= poids_maximal:
        combinaisons_possibles.append((combinaison, poids_total))

# comparaison du poids de chaque combinaison pour trouver le plus proche

plus_proche = 0
min = 1
poids_minimum = 0
for poids in combinaisons_possibles[1:]:
    if poids_maximal - poids[1] < min:
        min = poids_maximal - poids[1]
        plus_proche = poids[1]
        meilleure_combinaison = poids
print(reponse_b(meilleure_combinaison))

# ETAPE 3

print('c) Remplir la malle avec le maximum de mana.\n')

print(reponse_c(malle_c(fournitures_scolaires, poids_maximal), poids_maximal))
