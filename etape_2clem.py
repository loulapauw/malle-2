# coding: utf8

'''
Mini-projet "Harry se fait la malle"

Liste de fournitures scolaires

Auteur: Clémence Abrassart
'''
fournitures_scolaires = \
[{'Nom' : 'Manuel scolaire', 'Poids' : 0.55, 'Mana' : 11},
{'Nom' : 'Baguette magique', 'Poids' : 0.085, 'Mana' : 120},
{'Nom' : 'Chaudron', 'Poids' : 2.5, 'Mana' : 2},
{'Nom' : 'Boîte de fioles', 'Poids' : 1.2, 'Mana' : 4},
{'Nom' : 'Téléscope', 'Poids' : 1.9, 'Mana' : 6},
{'Nom' : 'Balance de cuivre', 'Poids' : 1.3, 'Mana' : 3},
{'Nom' : 'Robe de travail', 'Poids' : 0.5, 'Mana' : 8},
{'Nom' : 'Chapeau pointu', 'Poids' : 0.7, 'Mana' : 9},
{'Nom' : 'Gants', 'Poids' : 0.6, 'Mana' : 25},
{'Nom' : 'Cape', 'Poids' : 1.1, 'Mana' : 13}]

poids_maximal = 4


def remplir_b(liste):
    max = 4
    diff_fin = 4
    poids = 0
    caisse_fin = []
    for i in range(10):
        caisse = []
        for element in liste[i:]:
            if poids + element['Poids'] <= 4:
                caisse.append(element)
                poids += element['Poids']
        difference = max - poids
        if diff_fin < difference:
            diff_fin = poids
            caisse_fin = caisse
    return caisse_fin


def reponse (caisse):
    poids_de_la_malle = 0
    texte = 'La malle contient : '
    for element in caisse :
        texte += f"un(e) {element['Nom']}, "
        poids_de_la_malle += element['Poids']
    texte += f'et pèse {poids_de_la_malle}'
    return texte
print(reponse(remplir_b(fournitures_scolaires)))

