fournitures_scolaires = \
[{'Nom' : 'Manuel scolaire', 'Poids' : 0.55, 'Mana' : 11},
{'Nom' : 'Baguette magique', 'Poids' : 0.085, 'Mana' : 120},
{'Nom' : 'Chaudron', 'Poids' : 2.5, 'Mana' : 2},
{'Nom' : 'Boîte de fioles', 'Poids' : 1.2, 'Mana' : 4},
{'Nom' : 'Téléscope', 'Poids' : 1.9, 'Mana' : 6},
{'Nom' : 'Balance de cuivre', 'Poids' : 1.3, 'Mana' : 3},
{'Nom' : 'Robe de travail', 'Poids' : 0.5, 'Mana' : 8},
{'Nom' : 'Chapeau pointu', 'Poids' : 0.7, 'Mana' : 9},
{'Nom' : 'Gants', 'Poids' : 0.6, 'Mana' : 25},
{'Nom' : 'Cape', 'Poids' : 1.1, 'Mana' : 13}]

poids_maximal = 4


def tri_c(tab):
    """
    Fonction qui tri une liste par mana décroissant.
    Entrée: une liste
    Sortie: une liste triée 
    """
    for i in range (1, len(tab)):
        while tab[i]['Mana'] > tab[i - 1]['Mana'] and i > 0:
            tab[i], tab[i - 1] = tab[i - 1], tab[i]
            i = i - 1
    return tab

def malle_c(fourniture, max):
    """
    Fonction remplit la malle avec le plus de mana sans dépasser le poids.
    Entrée: une liste(fourniture) et un entier pour le poids maximum (max)
    Sortie: un tuple avec: une liste de ce qu'il y a dans la malle (malle_magique) et un 
    entier pour le mana qu'il y a dedans(mana)
    """
    malle_magique = []
    liste = tri_c(fourniture)
    poids = 0
    mana = 0
    for element in liste:
        if poids + element['Poids'] <= max:
            malle_magique.append(element)
            poids += element['Poids']
            mana += element['Mana']
    return malle_magique, mana



def reponse_c(liste, poids_max):
    texte = 'La malle avec le plus de mana contient:\n'
    for element in liste[0]:
        texte += f'- un(e) {element["Nom"]} qui à un mana de {element["Mana"]} \
et pèse {element["Poids"]}\n'
    return texte

print(reponse_c(malle_c(fournitures_scolaires, poids_maximal), poids_maximal))
